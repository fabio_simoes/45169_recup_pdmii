//
//  ViewController.swift
//  Conversor de moedas
//
//  Created by Developer on 11/10/2019.
//  Copyright © 2019 FabioSimoes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var euroconversor: UITextField!
    @IBOutlet var texto: UILabel!
    var numero = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        texto.text = "" //texto fica em branco ao inicio
       
    }

    @IBAction func conversor(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            numero = 0
            break
        case 1:
            numero = 1
            break
        case 2:
            numero = 2
            break
        default:
            break
     }
    }
    
    @IBAction func converter(_ sender: Any) {
        let num = Int(euroconversor.text!)
        switch numero{
        case 0:
            let convertido = Double(num!) * 1.10
            texto.text = "\(num!) euros equivale a \(convertido) dólares!"
            break
        case 1:
            let convertido = Double(num!) * 4.53
            texto.text = "\(num!) euros equivale a \(convertido) reais!"
            break
        case 2:
            let convertido = Double(num!) * 119.05
            texto.text = "\(num!) euros equivale a \(convertido) yen!"
            break
        default:
            break
        }
    }
}

